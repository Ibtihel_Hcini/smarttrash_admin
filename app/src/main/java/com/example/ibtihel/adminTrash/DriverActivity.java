package com.example.ibtihel.adminTrash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;


public class DriverActivity extends AppCompatActivity implements View.OnClickListener{

    private CardView pluscours, sendnotif;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driverhome);

        // defining cards
        pluscours = (CardView) findViewById(R.id.pluscours);
        sendnotif = (CardView) findViewById(R.id.sendnotif);


        // add action listner

        pluscours.setOnClickListener(this);
        sendnotif.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        Intent i ;
        switch ( v.getId()) {
            case  R.id.pluscours :
                i= new Intent(this, MapActivity.class);
                startActivity(i);
                break;

            case  R.id.sendnotif :

            i= new Intent(this, SendNotifActivity.class);
             startActivity(i);
                break;



        }


    }
}
