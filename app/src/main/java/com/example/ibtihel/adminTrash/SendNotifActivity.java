package com.example.ibtihel.adminTrash;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class SendNotifActivity extends AppCompatActivity {

    long index=0;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendnotif);

        Spinner deb = (Spinner) findViewById(R.id.deb);

        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(SendNotifActivity.this,
                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.debut));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deb.setAdapter(myAdapter);



        mDatabase = FirebaseDatabase.getInstance().getReference().child("notifications");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                    index =dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    public void reclam(View view) {



        Spinner deb = (Spinner) findViewById(R.id.deb);
        String dateDebut=deb.getSelectedItem().toString();
        String msg="Le début de la tournée sera après " +dateDebut+ " minutes. Si vous avez encore des déchets, veuillez les mettre dans les poubelles.";




        mDatabase.child(String.valueOf(index+1)).setValue(msg);

        Intent intent=new Intent(getApplicationContext(),SendNotif2Activity.class);
        intent.putExtra("msg", msg);
        startActivity(intent);



    }





}


