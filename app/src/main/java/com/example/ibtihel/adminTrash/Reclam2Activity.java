package com.example.ibtihel.adminTrash;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

public class Reclam2Activity extends AppCompatActivity implements View.OnClickListener{


    private CardView bnCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reclam2);

        Intent intent = getIntent();
        if (intent != null) {
            String str = "";
            if (intent.hasExtra("nomClient")) {
                str = intent.getStringExtra("nomClient");
            }

            TextView textView = (TextView) findViewById(R.id.nomCL);
            textView.setText(" Merci " + str + ",");


        }


        bnCard = (CardView) findViewById(R.id.homeBtn);
        bnCard.setOnClickListener(this);

    }


    public void onClick(View v) {
        Intent   i= new Intent(this, AdminActivity.class);
                startActivity(i);

    }






}