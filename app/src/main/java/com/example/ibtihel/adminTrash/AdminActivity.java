package com.example.ibtihel.adminTrash;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;


public class AdminActivity extends AppCompatActivity implements View.OnClickListener{

    private CardView see_reclam, infos_card;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adminhome);

        // defining cards
        see_reclam = (CardView) findViewById(R.id.see_reclamations);
        infos_card = (CardView) findViewById(R.id.info_card);

        // add action listner

        see_reclam.setOnClickListener(this);
        infos_card.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Intent i ;
        switch ( v.getId()) {
            case  R.id.see_reclamations :
                i= new Intent(this, SeeReclamActivity.class);
                startActivity(i);
                break;

            case  R.id.info_card :
               i= new Intent(this, InfoActivity.class);
                startActivity(i);
                break;


        }


    }
}
