package com.example.ibtihel.adminTrash.directionhelpers;



public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
