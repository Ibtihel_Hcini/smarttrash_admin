package com.example.ibtihel.adminTrash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SeeReclamActivity extends AppCompatActivity implements View.OnClickListener{

    private CardView bnCard;
    long index=0;
    DatabaseReference mDatabase;
    ListView listView;

    //Array list
    ArrayList<String> arrayList=new ArrayList<>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seereclam);


        bnCard = (CardView) findViewById(R.id.homeB);
        bnCard.setOnClickListener(this);

        //get the list of reclamations
        mDatabase = FirebaseDatabase.getInstance().getReference().child("reclamations");

        adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        listView = (ListView) findViewById(R.id.ListDesReclam);
        listView.setAdapter(adapter);


        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                Reclamation reclamation=dataSnapshot.getValue(Reclamation.class);

                String text="Message: "+reclamation.message +"\n"
                            +"De la part de: "+ reclamation.prenom+" "+reclamation.nom+"\n"
                            +"Tel: "+reclamation.tel+ "\n"
                            +"Zone: "+reclamation.zone;
                arrayList.add(text);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });







        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                    index =dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



    public void onClick(View v) {
        Intent   i= new Intent(this, AdminActivity.class);
        startActivity(i);

    }
}
