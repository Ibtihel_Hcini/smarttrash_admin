package com.example.ibtihel.adminTrash;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ibtihel.adminTrash.directionhelpers.FetchURL;
import com.example.ibtihel.adminTrash.directionhelpers.TaskLoadedCallback;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
     * An activity that displays a Google map with a marker (pin) to indicate a particular location.
     */
    public class MapActivity extends AppCompatActivity
            implements OnMapReadyCallback, TaskLoadedCallback {

    LatLng a = new LatLng(34.842094, 10.760394);
    LatLng b = new LatLng(34.824960, 10.746024);
    LatLng[] TwoMarkersRed={a,b};
    int redNumber=0;

    private GoogleMap mMap;
    Button getDirection;
    private MarkerOptions place1, place2;
    private Polyline currentPolyline;



    //zone 1 :
    LatLng t1_1 = new LatLng(34.842094, 10.760394);
    LatLng t1_2 = new LatLng(34.841240, 10.758709);
    LatLng t1_3 = new LatLng(34.839039, 10.758655);
    LatLng t1_4 = new LatLng(34.839198, 10.760878);
    LatLng t1_5 = new LatLng(34.836926, 10.760357);
    LatLng t1_6 = new LatLng(34.835762, 10.761349);

    //zone 2 :
    LatLng t2_1 = new LatLng(34.839480, 10.755873);
    LatLng t2_2 = new LatLng(34.838441, 10.754047);
    LatLng t2_3 = new LatLng(34.838203, 10.756945);
    LatLng t2_4 = new LatLng(34.835729, 10.754463);
    LatLng t2_5 = new LatLng(34.835021, 10.757286);
    LatLng t2_6 = new LatLng(34.834783, 10.759454);
    LatLng t2_7 = new LatLng(34.833999, 10.758048);

    //zone 3 :
    LatLng t3_1 = new LatLng(34.832042, 10.761600);
    LatLng t3_2 = new LatLng(34.833946, 10.762653);
    LatLng t3_3 = new LatLng(34.832553, 10.759208);
    LatLng t3_4 = new LatLng(34.830900, 10.757305);
    LatLng t3_5 = new LatLng(34.830809, 10.763058);
    LatLng t3_6 = new LatLng(34.830608, 10.761636);
    LatLng t3_7 = new LatLng(34.830234, 10.760040);
    LatLng t3_8 = new LatLng(34.829328, 10.758944);
    LatLng t3_9 = new LatLng(34.828767, 10.759911);
    LatLng t3_10 = new LatLng(34.828200, 10.758405);
    LatLng t3_11 = new LatLng(34.827704, 10.760111);
    LatLng t3_12 = new LatLng(34.826402, 10.760970);
    LatLng t3_13 = new LatLng(34.825626, 10.762666);
    LatLng t3_14 = new LatLng(34.825143, 10.760538);
    LatLng t3_15 = new LatLng(34.824921, 10.761812);

    //zone 4 :
    LatLng t4_1 = new LatLng(34.829761, 10.755293);
    LatLng t4_2 = new LatLng(34.829337, 10.755648);
    LatLng t4_3 = new LatLng(34.828613, 10.756570);
    LatLng t4_4 = new LatLng(34.827962, 10.755142);
    LatLng t4_5 = new LatLng(34.827071, 10.752700);
    LatLng t4_6 = new LatLng(34.826474, 10.753160);
    LatLng t4_7 = new LatLng(34.826474, 10.754436);
    LatLng t4_8 = new LatLng(34.825711, 10.755163);
    LatLng t4_9 = new LatLng(34.825060, 10.753966);
    LatLng t4_10 = new LatLng(34.824540, 10.755266);
    LatLng t4_11 = new LatLng(34.825848, 10.757382);
    LatLng t4_12 = new LatLng(34.825210, 10.758945);
    LatLng t4_13 = new LatLng(34.823909, 10.758396);
    LatLng t4_14 = new LatLng(34.823079, 10.756385);

    //zone 5:
    LatLng t5_1 = new LatLng(34.835921, 10.747341);
    LatLng t5_2 = new LatLng(34.835107, 10.746587);
    LatLng t5_3 = new LatLng(34.835629, 10.748441);
    LatLng t5_4 = new LatLng(34.834201, 10.751619);
    LatLng t5_5 = new LatLng(34.832733, 10.752266);
    LatLng t5_6 = new LatLng(34.830964, 10.749756);

    //zone 6:
    LatLng t6_1 = new LatLng(34.826775, 10.750872);
    LatLng t6_2 = new LatLng(34.826617, 10.748823);
    LatLng t6_3 = new LatLng(34.825912, 10.749436);
    LatLng t6_4 = new LatLng(34.825630, 10.747161);
    LatLng t6_5 = new LatLng(34.824978, 10.747773);
    LatLng t6_6 = new LatLng(34.824960, 10.746024);

    //zone 7:
    LatLng t7_1 = new LatLng(34.825434, 10.743721);
    LatLng t7_2 = new LatLng(34.826137, 10.740004);
    LatLng t7_3= new LatLng(34.824775, 10.738618);
    LatLng t7_4 = new LatLng(34.824770, 10.740816);
    LatLng t7_5 = new LatLng(34.824110, 10.739645);
    LatLng t7_6 = new LatLng(34.821793, 10.740443);

    final LatLng[] markers = {t1_1, t1_2, t1_3, t1_4, t1_5, t1_6,
            t2_1, t2_2, t2_3, t2_4, t2_5, t2_6,t2_7,
            t3_1, t3_2, t3_3, t3_4, t3_5, t3_6,t3_7,t3_8, t3_9, t3_10, t3_11, t3_12, t3_13,t3_14,t3_15,
            t4_1, t4_2, t4_3, t4_4, t4_5, t4_6,t4_7,t4_8, t4_9, t4_10, t4_11, t4_12, t4_13,t4_14,
            t5_1, t5_2, t5_3, t5_4, t5_5, t5_6,
            t6_1, t6_2, t6_3, t6_4, t6_5, t6_6,
            t7_1, t7_2, t7_3, t7_4, t7_5, t7_6 };

    final String[] markers_String = {
            "t1_1","t1_2","t1_3","t1_4","t1_5","t1_6",
            "t2_1","t2_2","t2_3","t2_4","t2_5","t2_6","t2_7",
            "t3_1","t3_2","t3_3","t3_4","t3_5","t3_6","t3_7","t3_8","t3_9","t3_10","t3_11","t3_12","t3_13","t3_14","t3_15",
            "t4_1","t4_2","t4_3","t4_4","t4_5","t4_6","t4_7","t4_8","t4_9","t4_10","t4_11","t4_12","t4_13","t4_14",
            "t5_1","t5_2","t5_3","t5_4","t5_5","t5_6",
            "t6_1","t6_2","t6_3","t6_4","t6_5","t6_6",
            "t7_1","t7_2","t7_3","t7_4","t7_5","t7_6"};

















        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_map);

            //##################################

            // DatabaseReference
            final DatabaseReference mDatabase;

            mDatabase = FirebaseDatabase.getInstance().getReference();

            for (int i=0;i<markers.length;i++)
            {
                final int j=i;
                if(redNumber==1)
                    redNumber++;



                mDatabase.child("VirtualTrashs").child(markers_String[i].toString()).addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue().toString();
                        if (value.equals("1"))
                        {
                            if(redNumber==0)
                            {
                                TwoMarkersRed[0]=markers[j];
                                redNumber++;
                            }
                            if(redNumber==3)
                            {
                                redNumber++;
                                TwoMarkersRed[1]=markers[j];
                            }

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        throw firebaseError.toException();

                    }
                });


            }

            //##################################

            //-----------
            getDirection = findViewById(R.id.btnGetDirection);
            getDirection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new FetchURL(MapActivity.this).execute(getUrl(new MarkerOptions().position(TwoMarkersRed[0]).getPosition(), new MarkerOptions().position(TwoMarkersRed[1]).getPosition(), "driving"), "driving");


                }
            });


            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);


            //------------
            mapFragment.getMapAsync(this);
        }

        @Override
        public void onMapReady(final GoogleMap googleMap) {

            mMap = googleMap;
            // Add a marker in Sydney, Australia,
            // and move the map's camera to the same location.
            LatLng OnsCity = new LatLng(34.8327762, 10.7555032);












            // DatabaseReference
            final DatabaseReference mDatabase;
            mDatabase = FirebaseDatabase.getInstance().getReference();
            for (int i=0;i<markers.length;i++)
            {
                final int j=i;
                mDatabase.child("VirtualTrashs").child(markers_String[i].toString()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue().toString();
                        if (value.equals("1"))
                            mMap.addMarker(new MarkerOptions().position(markers[j]).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
                        else
                            mMap.addMarker(new MarkerOptions().position(markers[j]).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
                    }

                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        throw firebaseError.toException();

                    }
                });


            }

            CameraUpdate cameraPosition = CameraUpdateFactory.newLatLngZoom(OnsCity, 14);
            googleMap.moveCamera(cameraPosition);
            //googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

           //mMap.addMarker(place1);
            //mMap.addMarker(place2);

        }

        private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId)
        {
            Drawable vectorDrawable= ContextCompat.getDrawable(context,vectorResId);
            vectorDrawable.setBounds(0,0,vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
            Bitmap bitmap=Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas=new Canvas(bitmap);
            vectorDrawable.draw(canvas);
            return BitmapDescriptorFactory.fromBitmap(bitmap);
        }




    //functions for drawing the route
    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + getString(R.string.google_maps_key);
        return url;
    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }

    }
