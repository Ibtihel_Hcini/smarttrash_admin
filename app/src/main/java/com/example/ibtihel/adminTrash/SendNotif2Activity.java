package com.example.ibtihel.adminTrash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SendNotif2Activity extends AppCompatActivity implements View.OnClickListener{


    private CardView bnCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendnotif2);

        Intent intent = getIntent();
        if (intent != null) {
            String str = "";
            if (intent.hasExtra("msg")) {
                str = intent.getStringExtra("msg");
            }

            TextView textView = (TextView) findViewById(R.id.notifSent);
            textView.setText(str);


        }


        bnCard = (CardView) findViewById(R.id.homeBtn);
        bnCard.setOnClickListener(this);

    }


    public void onClick(View v) {
        Intent   i= new Intent(this, DriverActivity.class);
                startActivity(i);

    }






}